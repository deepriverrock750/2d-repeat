//http://pixelartmaker.com/art/9d5278583d9297d.png
//https://freesound.org/people/jammerboy70/sounds/399597/
//http://jlongster.github.io/canvas-game-bootstrap/
//http://www.williammalone.com/articles/create-html5-canvas-javascript-sprite-animation/
//http://www.lostdecadegames.com/how-to-make-a-simple-html5-canvas-game/
//http://blog.sklambert.com/html5-canvas-game-2d-collision-detection/
//http://draeton.github.io/stitches/
//https://bitbucket.org/johnloane/2dgameengine1617/commits/ff9954d9e9810ec32a149f90a93b4035bf7c46df


var MainGame = function()
{
	this.canvas = document.getElementById('game-canvas'),
	this.context = this.canvas.getContext('2d'),
	this.fpsElement = document.getElementById('fps'),
        
        //Music
        this.soundAndMusicElement = document.getElementById("maingame-sound-and-music"),
        this.musicElement = document.getElementById("music"),
        this.musicCheckboxElement = document.getElementById("musicCheckbox"),
        this.musicOn = this.musicCheckboxElement.checked;

//        //Sound
//        this.soundCheckboxElement = document.getElementById('sound-checkbox');
//        this.audioSprites = document.getElementById('maingame-audio-sprites');
//        this.soundOn = this.soundCheckboxElement.checked;
        
	//Game states
        this.paused = false,
        this.PAUSED_CHECK_INTERVAL = 200, //time in milliseconds
        this.pauseStartTime,
        this.windowHasFocus = true,
        this.gameStarted = true,
        this.gameCompleted = false,
        this.gameOver = false,
        this.lives = 3;
        this.DEFAULT_SCORE = 0,
        this.score = 0;
        this.high_score = localStorage.getItem("highscore");

	//Images
        this.spritesheet = new Image(),
	this.background = new Image(),
	this.player = new Image(),
        this.zombie = new Image(),
        this.bullet = new Image();

	//Time
	this.lastAnimationFrameTime = 0,
	this.lastFpsUpdateTime = 0,
	this.fps = 60;

        //MUSIC
        this.maingameSound = new Audio();
        this.maingameSound.src = "sounds/maingameSound.mp3";
        this.maingameSound.volume = 0.05;

        //Sprite Scrolling
        this.STARTING_SPRITE_OFFSET = 80,
        this.spriteOffset = this.STARTING_SPRITE_OFFSET,
        this.spriteVelocity = this.spriteOffset;
        
        this.INITIAL_PLAYER_X = this.canvas.width/2-380;
        this.INITIAL_PLAYER_Y = this.canvas.height/2-60;
        this.playerX = this.INITIAL_PLAYER_X;
        this.playerY = this.INITIAL_PLAYER_Y;
        this.playerVelocity = 8;
        this.bulletX = this.playerX;
        this.bulletY = this.playerY;
        this.bullet.visible = false;
        this.bulletSpeed = 10;
        
	//Key codes
	this.pressingDown = false;
        this.pressingUp = false;
        this.pressingLeft = false;
        this.pressingRight = false;
        
        this.bulletfired = false;
        
        // Sprite sheet cells................................................

        this.PLAYER_CELLS_WIDTH = 255; // pixels
        this.PLAYER_CELLS_HEIGHT = 170;
    
        this.ZOMBIE_CELLS_WIDTH = 293; // pixels
        this.ZOMBIE_CELLS_HEIGHT = 316;
        
        this.bulletCells = [
        {   left:5, top: 5,
            width: 435, heigth: 316}
        ],
                
        this.playerCells = [
        { left: 564, top: 5, 
            width: this.PLAYER_CELLS_WIDTH, height: this.PLAYER_CELLS_HEIGHT }
        ],
        
        this.zombieCells = [
        { left: 445,   top: 180, 
           width: this.ZOMBIE_CELLS_WIDTH, height: this.ZOMBIE_CELLS_HEIGHT },

        { left: 5,  top: 501, 
           width: this.ZOMBIE_CELLS_WIDTH, height: this.ZOMBIE_CELLS_HEIGHT },

        { left: 303, top: 501, 
           width: this.ZOMBIE_CELLS_WIDTH, height: this.ZOMBIE_CELLS_HEIGHT },

        { left: 601, top: 501, 
           width: this.ZOMBIE_CELLS_WIDTH, height: this.ZOMBIE_CELLS_HEIGHT },

        { left: 825, top: 5, 
           width: this.ZOMBIE_CELLS_WIDTH, height: this.ZOMBIE_CELLS_HEIGHT },
       
        { left: 899, top: 326, 
           width: this.ZOMBIE_CELLS_WIDTH, height: this.ZOMBIE_CELLS_HEIGHT },

        { left: 899, top: 647, 
           width: this.ZOMBIE_CELLS_WIDTH, height: this.ZOMBIE_CELLS_HEIGHT },

        { left: 5, top: 968, 
           width: this.ZOMBIE_CELLS_WIDTH, height: this.ZOMBIE_CELLS_HEIGHT },

        { left: 303, top: 968, 
           width: this.ZOMBIE_CELLS_WIDTH, height: this.ZOMBIE_CELLS_HEIGHT }
    ],
        
        //sprite
        this.sprites = [],
        this.zombies = [];

        // Sprite Behaviours
        this.moveBehaviour = 
        {
            execute: function(sprite, now, fps, lastAnimationFrameTime)
            {
                if(game.started === false)
                {
                    return;
                }
            }
        };
        
        this.collideBehaviour={
      //Three step process
      //1. What should we consider for collision?
      //2. Is there a collision
      //3. React to the collision
      isCandidateForCollision: function(sprite, otherSprite)
      {
        var s, o;
        s = sprite.calculateCollisionRectangle(),
        o = otherSprite.calculateCollisionRectangle();

        candidate =  o.left < s.right && sprite !== otherSprite &&
        sprite.visible && otherSprite.visible && !sprite.exploding && !otherSprite.exploding;
        return candidate;
      },

      didCollide: function(sprite, otherSprite, context)
      {
        var o = otherSprite.calculateCollisionRectangle(),
            r = sprite.calculateCollisionRectangle();
            return this.didRunnerCollideWithOtherSprite(r, o, context);
         

      },

      didRunnerCollideWithSnailBomb: function (r, o, context) {
         // Determine if the center of the snail bomb lies
         // within the runner's bounding box.

         context.beginPath();
         context.rect(r.left + game.spriteOffset, 
                      r.top, r.right - r.left, r.bottom - r.top);

         return context.isPointInPath(o.centerX, o.centerY);
      },

      didRunnerCollideWithOtherSprite: function (r, o, context) {
         // Determine if either of the runner's four corners or its
         // center lie within the other sprite's bounding box.

         context.beginPath();
         context.rect(o.left, o.top, o.right - o.left, o.bottom - o.top);

         return context.isPointInPath(r.left,  r.top)       ||
                context.isPointInPath(r.right, r.top)       ||

                context.isPointInPath(r.centerX, r.centerY) ||

                context.isPointInPath(r.left,  r.bottom)    ||
                context.isPointInPath(r.right, r.bottom);
      },

      processBadGuyCollision: function(sprite)
      {
         console.log("here");
         game.explode(sprite);
         game.shake();
         game.loseLife();
      },

      processCollision: function(sprite, otherSprite)
      {
        
        if('zombie' === otherSprite.type || 'bee' === otherSprite.type || 'snail bomb' === otherSprite.type)
        {
          //console.log("Bad collision");
          this.processBadGuyCollision(sprite);
        }
        
      },

      processAssetCollision: function (sprite) {
         sprite.visible = false; // sprite is the asset

         if (sprite.type === 'coin') {
            game.playSound(game.coinSound);
         }
         else {
            game.playSound(game.pianoSound);
         }
      },

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        var otherSprite;

        if ( ! game.playing) {
            return;
         }

        for(var i=0; i < game.sprites.length; ++i)
        {
          otherSprite = game.sprites[i];
          if(this.isCandidateForCollision(sprite, otherSprite))
          {
            if(this.didCollide(sprite, otherSprite, context))
            {
              this.processCollision(sprite, otherSprite);
            }
          }
        }
      }
    };
}; //End of constructor

MainGame.prototype = 
{
        createSprites : function()
        {
            this.createPlayerSprite();
            this.createZombieSprite();
            //this.createBullet();
            //Add all of the sprites to a single array
            this.addSpritesToSpriteArray();
        },
        
        addSpritesToSpriteArray: function()
	{
                    //console.log("zombies: " + this.zombies.length);
		for(var i =0; i < this.zombies.length; ++i)
		{
			this.sprites.push(this.zombies[i]);
		}
                this.sprites.push(this.bullet);
		this.sprites.push(this.player);
                console.log("Sprites: " + this.sprites.length);
	},
        
        createPlayerSprite: function()
	{
            this.player = new Sprite("player", new SpriteSheetArtist(this.spritesheet, this.playerCells), [this.collideBehaviour]);
            this.player.top = this.playerY;
            this.player.left = this.playerX;
	},
        
//        createBullet: function()
//        {
//            this.bullet = new Sprite("bullet", new SpriteSheetArtist(this.spritesheet, this.bulletCells), null);
//            this.bullet.top =  this.player.left + this.player.width;
//            this.bullet.left =  this.player.top + 45;
//            this.bullet.visible = false;
//        },
        
        createZombieSprite: function()
	{
            var numOfZombies = 20;
            for(var i = 0; i < numOfZombies; i++)
            {
                var ZOMBIE_DURATION = 150,
                    ZOMBIE_INTERVAL = 20;
                this.zombie = new Sprite("zombie", new SpriteSheetArtist(this.spritesheet, this.zombieCells),[new CycleBehaviour(ZOMBIE_DURATION, ZOMBIE_INTERVAL), this.collideBehaviour]);
                this.zombie.left = Math.floor((Math.random() * 1500) + 500);
                this.zombie.top = Math.floor((Math.random() * 275) + 0);
                this.zombies.push(this.zombie);
            }
            //this.sprites.push(this.zombie);
	},
        
        updateSprites: function(now)
        {
            var sprite;
            var zombieSpeed = 0.75;
            //var bulletSpeed = 1;
            console.log(this.sprites.length);
            for(var i=0; i<this.sprites.length; i++)
            {
                sprite = this.sprites[i];
                if(sprite.visible && this.isSpriteInView(sprite))
                {
                    this.context.translate(-sprite.hOffset, 0);
                    sprite.update(now, this.fps, this.context, this.lastAnimationFrameTime);
                    this.context.translate(sprite.hOffset, 0);
                    if(sprite.type === "zombie")
                    {
                        sprite.left -= zombieSpeed;
                    }
//                    else if(sprite.type === "bullet")
//                    {
//                        while(sprite.left > this.canvas.width && sprite.visible)
//                        {
//                            sprite.left += bulletSpeed;
//                        }
//                    }
                }
            }
        },
        
	//Animation
	animate: function(now)
	{
            if(game.paused)
            {
                setTimeout(function(){
                requestAnimationFrame(game.animate);
                game.musicElement.pause();
                }, game.PAUSED_CHECK_INTERVAL);
            }
            else
            {
                game.startMusic();
                fps = game.calculateFps(now);
                game.draw(now);
                game.lastAnimationFrameTime = now;
                requestAnimationFrame(game.animate);
            }
	},

	calculateFps: function(now)
	{
		var fps = 1/(now - game.lastAnimationFrameTime) * 1000;
		if(now - game.lastFpsUpdateTime > 1000)
		{
			game.lastFpsUpdateTime = now;
			game.fpsElement.innerHTML = fps.toFixed(0) + ' fps'; 
		}
		return fps;
	},

	initializeImages: function()
	{
            game.spritesheet.src = 'images/spritesheet.png';
            game.background.src = 'images/background.jpeg';
            game.bullet.src = 'images/background.png';
            //game.player.src = 'images/player.png';
            //game.zombie.src = 'images/zombieSpritesheet.png';

            this.background.onload = function(e)
            {
                    game.startGame();
            };
	},

	startGame: function()
	{
		window.requestAnimationFrame(game.animate);
	},
        
        reset: function()
        {
            this.INITIAL_PLAYER_X = this.canvas.width/2-380;
            this.INITIAL_PLAYER_Y = this.canvas.height/2-60;
            this.playerX = this.INITIAL_PLAYER_X;
            this.playerY = this.INITIAL_PLAYER_Y;
        },
        
        explode: function(sprite)
        {
            sprite.visible = false;
        },

	draw: function(now)
	{
            game.drawBackground();
            game.drawLives();
            game.drawScore();
            game.drawSprites();
            //game.drawBullet();
            //game.setOffsets();
            game.updatePosition(now);
            game.updateSprites(now);
            game.drawSprites();
            
	},
        
        drawLives: function()
        {
            
            this.context.fillStyle = "#E01C4F";
            this.context.font = "35px Aclonica";
            this.context.textAlign = "left";
            this.context.fillText("Lifes: " + this.lives, 0, 25);
        },
        
        drawScore: function()
        {
            this.context.fillStyle = "#E01C4F";
            this.context.font = "20 Aclonica";
            this.context.textAlign = "right";
            this.context.fillText("SCORE: " + this.score, this.canvas.width, this.canvas.height-5);
        },

	drawSprites: function()
        {
            var sprite;
            for(var i=0; i<this.sprites.length; i++)
            {
                sprite = this.sprites[i];
                if(sprite.visible && this.isSpriteInView(sprite))
                {
                    this.context.translate(-sprite.hOffset, 0);
                    sprite.draw(this.context);
                    this.context.translate(sprite.hOffset, 0);
                }
            }
        },

	isSpriteInView: function(sprite)
	{
		return sprite.left+sprite.width > sprite.hOffset && sprite.left < sprite.hOffset + this.canvas.width;
 	},

	drawBackground: function()
	{
		game.context.drawImage(game.background, 0, 0, 800, 400);
                //game.coxtext.drawImage(game.test, 0, 0);
	},

//        drawBullet: function() not working
//	{
//		game.context.drawImage(this.bullet, this.canvas.width/2, this.canvas.heigth/2);
//	},
        
        updateBullet: function()
        {
            while(this.bulletX > this.canvas.width)
            {
                this.bulletX += this.bulletSpeed;
            }
        },
        
        setOffsets: function(now)
        {
            game.setSpriteOffsets(now);
        },
        
        setSpriteOffsets: function (now) 
        {
            var sprite;
            // In step with platforms
            this.spriteOffset += this.spriteVelocity * (now - this.lastAnimationFrameTime) / 1000;

            for (var i=0; i < this.sprites.length-3; ++i) 
            {
                sprite = this.sprites[i];
                sprite.hOffset = this.spriteOffset;
            }
        },
        
        updatePosition: function (now)
        {
            if(this.pressingRight)
            {
                this.playerX += this.playerVelocity;
                this.player.left = this.playerX;
            }
            
            if(this.pressingLeft)
            {
                this.playerX -= this.playerVelocity;
                this.player.left = this.playerX;
            }
            
            if(this.pressingDown)
            {
                this.playerY += this.playerVelocity;
                this.player.top = this.playerY;
            }
            
            if(this.pressingUp)
            {
                this.playerY -= this.playerVelocity;
                this.player.top = this.playerY;
            }
            
            if(this.bulletfired)
            {
                game.fireBullet(now);
            }
            
            //enables canvas boundary
            if(this.playerX < 0)
            {
                this.playerX = 0;
            }
            if(this.playerX > this.canvas.width-730)
            {
                this.playerX = this.canvas.width - 730;
            }
            
            if(this.playerY < 0)
            {
                this.playerY = 0 + this.player.height/2;
            }
                
            
            if(this.playerY > this.canvas.height - this.player.height/2)
            {
                this.playerY = this.canvas.height - this.player.height/2;
                
            }
        },
        
        togglePaused: function()
        {
            var now = +new Date();
            this.paused = !this.paused;
            if(this.paused)
            {
                this.pauseStartTime = now;
                this.context.fillStyle = "White";
                this.context.textAlign = "center";
                this.context.font = "50px Aclonica";
                this.context.fillText("PAUSED", this.canvas.width/2, this.canvas.height/2);
            }
            else
            {
                    this.lastAnimationFrameTime += (now - this.pauseStartTime);
            }
        },
        
    fireBullet: function(now)
    {
        this.bullet.visible = true;
        console.log("Fire");
        while(!this.bullet.left > this.canvas.width)
        {
            this.bulletX += 10;
            console.log("bulletX " +this.bulletX);
            
        }
        game.resetBullet();
    },
    
    resetBullet: function()
    {
        this.bulletfired = false;
        this.bullet.visible = false;
    },
        
        startMusic: function()
        {
          var MUSIC_DELAY = 1000;
          setTimeout(function(){
            if(game.musicCheckboxElement.checked)
            {
              game.musicElement.play();
            }
            game.pollMusic();
          }, MUSIC_DELAY);
        },
        
        pollMusic: function()
        {
            var POLL_INTERVAL = 500,
                SOUNDTRACK_LENGTH = 132,
                timerID;

            timerID = setInterval( function()
            {
                if(game.musicElement.currentTime > SOUNDTRACK_LENGTH)
                {
                    clearInterval(timerID); //stop polling
                    restartMusic();
                }
            }, POLL_INTERVAL);
        },

        restartMusic: function()
        {
            game.musicElement.pause();
            game.musicElement.currentTime = 0;
            startMusic();
        },
        
        detectMobile: function () {
      game.mobile = 'ontouchstart' in window;
   },
   
    fitScreen: function () {
      var arenaSize = game.calculateArenaSize(
                         game.getViewportSize());

      game.resizeElementsToFitScreen(arenaSize.width, 
                                          arenaSize.height);
   },
   
    calculateArenaSize: function (viewportSize) {
      var DESKTOP_ARENA_WIDTH  = 800,  // Pixels
          DESKTOP_ARENA_HEIGHT = 520,  // Pixels
          arenaHeight,
          arenaWidth;

      arenaHeight = viewportSize.width * 
                    (DESKTOP_ARENA_HEIGHT / DESKTOP_ARENA_WIDTH);

      if (arenaHeight < viewportSize.height) { // Height fits
         arenaWidth = viewportSize.width;      // Set width
      }
      else {                                   // Height does not fit
         arenaHeight = viewportSize.height;    // Recalculate height
         arenaWidth  = arenaHeight *           // Set width
                      (DESKTOP_ARENA_WIDTH / DESKTOP_ARENA_HEIGHT);
      }

      if (arenaWidth > DESKTOP_ARENA_WIDTH) {  // Too wide
         arenaWidth = DESKTOP_ARENA_WIDTH;     // Limit width
      } 

      if (arenaHeight > DESKTOP_ARENA_HEIGHT) { // Too tall
         arenaHeight = DESKTOP_ARENA_HEIGHT;    // Limit height
      }

      return { 
         width:  arenaWidth, 
         height: arenaHeight 
      };
   },
   
   getViewportSize: function () {
      return { 
        width: Math.max(document.documentElement.clientWidth ||
               window.innerWidth || 0),  
               
        height: Math.max(document.documentElement.clientHeight ||
                window.innerHeight || 0)
      };
   },
   
   resizeElement: function (element, w, h) {
      console.log(element);
      element.style.width  = w + 'px';
      element.style.height = h + 'px';
   },

   resizeElementsToFitScreen: function (arenaWidth, arenaHeight) {
      game.resizeElement(
         document.getElementById('arena'), 
         arenaWidth, arenaHeight);
   },
};


var game = new MainGame();

game.musicCheckboxElement.addEventListener('change',
     function(e){
        game.musicOn = game.musicCheckboxElement.checked;
    if(game.musicOn){
      game.musicElement.play();
    }
    else{
      game.musicElement.pause();
    }
  });

//keyboard input
document.onkeydown = function(event, now)
{
    if(event.keyCode === 68)
    {
        game.pressingRight = true;
    }
    
    else if(event.keyCode === 83)
    {
        game.pressingDown = true;
    }
    
    else if(event.keyCode === 65)
    {
        game.pressingLeft = true;
    }
    
    else if(event.keyCode === 87)
    {
        game.pressingUp = true;
    }
    
    else if(event.keyCode === 32)
    {
        game.bulletfired = true;
    }   
    
    else if(event.keyCode === 80)
    {
        game.togglePaused();
    }    
},

document.onkeyup = function(event)
{
    if(event.keyCode === 68)
    {
        game.pressingRight = false;
    }
    
    else if(event.keyCode === 83)
    {
        game.pressingDown = false;
    }
    
    else if(event.keyCode === 65)
    {
        game.pressingLeft = false;
    }
    
    else if(event.keyCode === 87)
    {
        game.pressingUp = false;
    }
};

window.addEventListener('blur', function(e)
{
    game.windowHasFocus = false;
    if(!game.paused && !game.mainmenu && !game.GAME_OVER)
    {
            game.togglePaused(); //Pause the game
}});

window.addEventListener('focus', function(e)
{
    game.windowHasFocus = true;
});

game.initializeImages();
game.createSprites();

game.detectMobile();

if (game.mobile) 
(
    document.body.addEventListener("touchstart", function (e) 
    {
        if (e.target == game.canvas && game.gameStarted && !game.paused) 
        {
            game.shotFired = true;
            e.preventDefault();
        }
    })
 );
 
 if(game.mobile)
 {
    document.body.addEventListener("touchmove", function (e) 
    {
        if(game.touchX !== null && game.touchY !== null)
        {
            if(game.gameStarted && !game.paused)
            {
                if (e.target == game.canvas && e.touches[0].pageY > game.touchY) 
                {
                    game.DOWN = true;
                    e.preventDefault();
                }
                else
                {
                    game.DOWN = false;
                }

                if (e.target == game.canvas && e.touches[0].pageY < game.touchY) 
                {
                    game.UP = true;
                    e.preventDefault();
                }
                else
                {
                    game.UP = false;
                }

                if (e.target == game.canvas && e.touches[0].pageX > game.touchX) 
                {
                    game.RIGHT = true;
                    e.preventDefault();
                }
                else
                {
                    game.RIGHT = false;
                }

                if (e.target == game.canvas && e.touches[0].pageX < game.touchX) 
                {
                    game.LEFT = true;
                    e.preventDefault();
                }
                else
                {
                    game.LEFT = false;
                }
            }
        }
        game.touchX = e.touches[0].pageX;
        game.touchY = e.touches[0].pageY;
    }, false);
 };
 
if(game.mobile)
{
   document.body.addEventListener("touchend", function (e) 
   {
       game.LEFT = false;
       game.RIGHT = false;
       game.UP = false;
       game.DOWN = false;
       game.touchX = null;
       game.touchY = null;
       e.preventDefault();
   }
)};
 

game.fitScreen();
window.addEventListener("resize", game.fitScreen);
window.addEventListener("orientationchange", game.fitScreen);